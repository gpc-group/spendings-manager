package ru.pgc.spending_manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.modulith.core.ApplicationModules;

@SpringBootApplication
@ConfigurationPropertiesScan("ru.pgc.spending_manager.telegram_bot.config.properties")
public class SpendingsManager {


    public static void main(String[] args) {
        verifiesModularStructure();
        SpringApplication.run(SpendingsManager.class,args);
    }


    static void verifiesModularStructure() {
        var modules = ApplicationModules.of(SpendingsManager.class);
        modules.verify();
        modules.forEach(System.out::println);
    }

}
