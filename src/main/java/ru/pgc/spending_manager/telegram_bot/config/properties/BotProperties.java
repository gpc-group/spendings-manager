package ru.pgc.spending_manager.telegram_bot.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("telegram-bot")
public record BotProperties(String webhookPath, String botUsername, String botToken) {
}
