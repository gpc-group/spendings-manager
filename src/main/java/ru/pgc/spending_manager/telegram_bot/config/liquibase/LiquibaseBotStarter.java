package ru.pgc.spending_manager.telegram_bot.config.liquibase;

import liquibase.exception.LiquibaseException;
import liquibase.integration.spring.SpringLiquibase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "liquibase.bot", name = "enabled", havingValue = "true")
public class LiquibaseBotStarter implements InitializingBean {

    private final SpringLiquibase liquibase;
    private final LiquibaseBotProperties liquibaseProperties;

    @Override
    public void afterPropertiesSet() throws Exception {
        if (liquibaseProperties.isEnabled()) {
            liquibase.setChangeLog(liquibaseProperties.getChangeLog());

            try {
                liquibase.afterPropertiesSet();
            } catch (LiquibaseException exception) {
                log.error("Error running Liquibase {}", exception.getMessage(), exception);
                throw new IllegalStateException("Ошибка при выполнении Liquibase", exception);
            } catch (Exception exception) {
                log.error("Database initialization error with Liquibase {}", exception.getMessage(), exception);
                throw new IllegalStateException("Не удалось инициализировать базу данных с помощью Liquibase", exception);
            }
        }
    }
}



