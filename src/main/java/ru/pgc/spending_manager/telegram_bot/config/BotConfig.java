package ru.pgc.spending_manager.telegram_bot.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.api.methods.updates.SetWebhook;
import ru.pgc.spending_manager.telegram_bot.config.properties.BotProperties;

@Configuration
@ConditionalOnProperty(prefix = "telegram-bot", name = "enabled", havingValue = "true", matchIfMissing = true)
public class BotConfig {

    @Bean
    public DefaultBotOptions defaultBotOptions() {

        return new DefaultBotOptions();
    }

    @Bean
    public SetWebhook setWebhook(BotProperties properties) {

        return SetWebhook.builder()
                .url(properties.webhookPath())
                .build();
    }
}