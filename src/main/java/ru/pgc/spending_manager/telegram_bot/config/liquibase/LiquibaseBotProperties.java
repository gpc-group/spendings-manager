package ru.pgc.spending_manager.telegram_bot.config.liquibase;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "liquibase.bot")
public class LiquibaseBotProperties {
    private String changeLog;
    private boolean enabled;
}