package ru.pgc.spending_manager.telegram_bot;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;
import org.springframework.web.util.UriComponentsBuilder;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.updates.SetWebhook;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.starter.SpringWebhookBot;
import ru.pgc.spending_manager.telegram_bot.config.properties.BotProperties;

import java.net.URI;

@Slf4j
@Component
@ConditionalOnProperty(prefix = "telegram-bot", name = "enabled", havingValue = "true", matchIfMissing = true)
public final class SpendingsManagerBot extends SpringWebhookBot {

    private final String webhookPath;
    private final String botToken;
    private final String botUsername;


    public SpendingsManagerBot(BotProperties properties,
                               DefaultBotOptions defaultBotOptions,
                               SetWebhook setWebhook) {

        super(defaultBotOptions, setWebhook, properties.botToken());
        this.webhookPath = properties.webhookPath();
        this.botUsername = properties.botUsername();
        this.botToken = properties.botToken();
    }

    @PostConstruct
    void init() {

        URI webhookRegistrationURI = getWebhookRegistrationURI();

        registerWebhook(webhookRegistrationURI);

    }

    private void registerWebhook(URI webhookRegistrationURI) {

        RestClient restClient = RestClient.create();

        log.info("Sending webhook registration request to telegram API");
        restClient.get()
                .uri(webhookRegistrationURI)
                .retrieve()
                .onStatus(
                        HttpStatusCode::isError,
                        (request, response) -> log.warn("Webhook registration has failed: {}", response.getStatusText()))
                .onStatus(
                        HttpStatusCode::is2xxSuccessful,
                        (request, response) -> log.info("Webhook registration has been successful")
                )
                .toBodilessEntity();
    }

    private URI getWebhookRegistrationURI() {

        return UriComponentsBuilder.newInstance()
                .scheme("https")
                .host("api.telegram.org")
                .pathSegment("bot" + this.botToken, "setWebhook")
                .queryParam("url", this.webhookPath)
                .build()
                .toUri();
    }

    @Override
    public BotApiMethod<?> onWebhookUpdateReceived(Update update) {

        //TODO Реализовать в дальнейшем
        return null;
    }

    @Override
    public String getBotPath() {

        return this.webhookPath;
    }

    @Override
    public String getBotUsername() {

        return this.botUsername;
    }
}
