package ru.pgc.spending_manager.telegram_bot.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.pgc.spending_manager.telegram_bot.SpendingsManagerBot;

@RestController
@Slf4j
@ConditionalOnProperty(prefix = "telegram-bot", name = "enabled", havingValue = "true", matchIfMissing = true)
public class WebhookController {
    private final SpendingsManagerBot spendingsManagerBot;

    public WebhookController(SpendingsManagerBot spendingsManagerBot) {

        this.spendingsManagerBot = spendingsManagerBot;
    }

    @PostMapping("${telegram-bot.endpoint-url}")
    public BotApiMethod<?> onUpdateReceived(@RequestBody Update update) {

        log.info("TG bot received an update");

        return spendingsManagerBot.onWebhookUpdateReceived(update);
    }
}
