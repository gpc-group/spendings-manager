package ru.pgc.spending_manager.servise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pgc.spending_manager.servise.entities.Category;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CategoryRepositories extends JpaRepository<Category, UUID> {
    List<Category> findAll();
    List<Category> findAllByIsDefaultIsTrue();
    Optional<Category> findCategoryByNameCategoryIgnoreCase(String nameCategory);

}
