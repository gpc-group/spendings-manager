package ru.pgc.spending_manager.servise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pgc.spending_manager.servise.entities.Spending;

@Repository
public interface SpendingRepositories extends JpaRepository<Spending, Long> {

}
