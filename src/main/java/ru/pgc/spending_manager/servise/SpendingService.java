package ru.pgc.spending_manager.servise;

import ru.pgc.spending_manager.servise.dto.SpendingDtoRequest;
import ru.pgc.spending_manager.servise.dto.SpendingDtoResponse;

public interface SpendingService {

    SpendingDtoResponse addSpending(SpendingDtoRequest spendingDtoRequest);
}
