package ru.pgc.spending_manager.servise.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@Table(name = "category", schema = "spending_manager")
public class Category {

    @Id
    @Column(name = "category_id")
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID categoryId;

    @Column(name = "name_category")
    private String nameCategory;

    @Column(name = "is_default")
    private boolean isDefault;

    @ManyToMany(mappedBy = "categories", fetch = FetchType.LAZY)
    private List<User> users;

    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
    private List<Spending> spendings;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return Objects.equals(categoryId, category.categoryId) && Objects.equals(nameCategory, category.nameCategory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categoryId, nameCategory);
    }

    @Override
    public String toString() {
        return "Category{" +
                "isDefault=" + isDefault +
                ", nameCategory='" + nameCategory + '\'' +
                '}';
    }
}
