package ru.pgc.spending_manager.servise.excepcions;

public class UserNotFoundException extends RuntimeException{
    public UserNotFoundException(String message){
        super(message);
    }

}
