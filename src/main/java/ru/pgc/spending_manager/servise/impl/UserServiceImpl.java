package ru.pgc.spending_manager.servise.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pgc.spending_manager.servise.UserService;
import ru.pgc.spending_manager.servise.dto.UserDtoResponse;
import ru.pgc.spending_manager.servise.entities.Category;
import ru.pgc.spending_manager.servise.entities.User;
import ru.pgc.spending_manager.servise.excepcions.UserNotFoundException;
import ru.pgc.spending_manager.servise.repository.CategoryRepositories;
import ru.pgc.spending_manager.servise.repository.UserRepository;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final CategoryRepositories categoryRepositories;

    @Override
    @Transactional
    public UUID createUser() {
        log.info("Creating user");

        User user = createUserAndSetRandomUuid();
        user.setCategories(getDefaultCategories());
        User savedUser = userRepository.save(user);
        log.info("Categories {} ", savedUser.getCategories());
        log.info("User created with id {}", user.getId());

        return savedUser.getId();
    }

    private User createUserAndSetRandomUuid() {
        User user = new User();
        UUID uuid = UUID.randomUUID();
        user.setId(uuid);

        return user;
    }

    private List<Category> getDefaultCategories() {
        log.info("Getting default categories from db");

        return categoryRepositories.findAllByIsDefaultIsTrue();

    }

    @Override
    @Transactional
    public UserDtoResponse findById(UUID id) {
        log.info("Finding user by id {}", id);

        User userFromDb = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User not found with id " + id));
        UserDtoResponse userDtoResponse = new UserDtoResponse();
        userDtoResponse.setId(userFromDb.getId());

        return userDtoResponse;
    }

    @Override
    public List<Category> getAllUserCategories(String userId) {
        log.info("Getting all categories from db on user id {}", userId);

        User user = userRepository.findById(UUID.fromString(userId))
                .orElseThrow(() -> new UserNotFoundException("User not found with id " + userId));

        return user.getCategories();
    }
}
