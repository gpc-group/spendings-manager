package ru.pgc.spending_manager.servise.impl;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pgc.spending_manager.servise.SpendingService;
import ru.pgc.spending_manager.servise.dto.SpendingDtoRequest;
import ru.pgc.spending_manager.servise.dto.SpendingDtoResponse;
import ru.pgc.spending_manager.servise.entities.Category;
import ru.pgc.spending_manager.servise.entities.Spending;
import ru.pgc.spending_manager.servise.entities.User;
import ru.pgc.spending_manager.servise.excepcions.UserNotFoundException;
import ru.pgc.spending_manager.servise.repository.SpendingRepositories;
import ru.pgc.spending_manager.servise.repository.UserRepository;

import java.util.NoSuchElementException;

@Service
@AllArgsConstructor
public class SpendingServiceImpl implements SpendingService {

    private final UserRepository userRepository;
    private final SpendingRepositories spendingRepositories;

    @Override
    @Transactional
    public SpendingDtoResponse addSpending(SpendingDtoRequest spendingDtoRequest) {

        User userFromDb = userRepository.findUserById(spendingDtoRequest.getUserId())
                .orElseThrow(() -> new UserNotFoundException("User not found"));

        Category userCategory = userFromDb.getCategories()
                .stream()
                .filter(category -> category.getNameCategory().equalsIgnoreCase(spendingDtoRequest.getCategoryName()))
                .findAny()
                .orElseThrow(NoSuchElementException::new);

        Spending savedSpending = buildNewSpending(spendingDtoRequest, userFromDb, userCategory);

        return mapToSpendingDtoResponse(savedSpending);

    }

    private Spending buildNewSpending(SpendingDtoRequest spendingDtoRequest, User userFromDb, Category userCategory) {
        Spending newSpending = new Spending();
        newSpending.setUser(userFromDb);
        newSpending.setCategory(userCategory);
        newSpending.setSpendingDate(spendingDtoRequest.getExpenseDate());
        newSpending.setAmount(spendingDtoRequest.getExpenseSize());
        return spendingRepositories.save(newSpending);
    }

    private SpendingDtoResponse mapToSpendingDtoResponse(Spending newSpending) {
        SpendingDtoResponse spendingDtoResponse = new SpendingDtoResponse();
        spendingDtoResponse.setId(newSpending.getId());
        spendingDtoResponse.setCategoryName(newSpending.getCategory().getNameCategory());
        spendingDtoResponse.setExpenseDate(newSpending.getSpendingDate());
        spendingDtoResponse.setExpenseSize(newSpending.getAmount());

        return spendingDtoResponse;

    }
}