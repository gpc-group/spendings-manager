package ru.pgc.spending_manager.servise.impl;

import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.pgc.spending_manager.servise.CategoryService;
import ru.pgc.spending_manager.servise.dto.CategoryDtoResponse;
import ru.pgc.spending_manager.servise.entities.Category;
import ru.pgc.spending_manager.servise.entities.User;
import ru.pgc.spending_manager.servise.excepcions.UserNotFoundException;
import ru.pgc.spending_manager.servise.repository.CategoryRepositories;
import ru.pgc.spending_manager.servise.repository.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
@Slf4j
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepositories categoryRepositories;
    private final UserRepository userRepository;

    @Override
    @Transactional
    public CategoryDtoResponse addCategory(String userId, String nameCategory) {
        log.info("AddCategories called with userId: {}, nameCategory: {}", userId, nameCategory);

        UUID userUUID = UUID.fromString(userId);

        User userFromDb = userRepository.findUserById(userUUID)
                .orElseThrow(() -> new UserNotFoundException("User not found with id " + userId));
        Optional<Category> optionalCategory = categoryRepositories.findCategoryByNameCategoryIgnoreCase(nameCategory);

        CategoryDtoResponse categoryDtoResponse;
        if (optionalCategory.isEmpty()) {
            categoryDtoResponse = createNewCategory(nameCategory, userFromDb);
        } else {
            categoryDtoResponse = addUserToExistingCategory(nameCategory, optionalCategory, userFromDb);
        }

        return categoryDtoResponse;
    }

    private CategoryDtoResponse addUserToExistingCategory(String nameCategory, Optional<Category> optionalCategory, User userFromDb) {
        CategoryDtoResponse categoryDtoResponse = new CategoryDtoResponse();
        Category existingCategory = optionalCategory.get();
        if (existingCategory.getUsers().contains(userFromDb)) {
            log.info("User already has this category: {}", nameCategory);
        } else {
            log.info("Adding an existing category to a user: {}", nameCategory);
            existingCategory.getUsers().add(userFromDb);
            categoryRepositories.save(existingCategory);
        }
        categoryDtoResponse.setId(existingCategory.getCategoryId());
        categoryDtoResponse.setCategoryName(existingCategory.getNameCategory());

        return categoryDtoResponse;
    }

    private CategoryDtoResponse createNewCategory(String nameCategory, User userFromDb) {
        CategoryDtoResponse categoryDtoResponse = new CategoryDtoResponse();
        log.info("Add new category with name: {}", nameCategory);
        Category newCategory = new Category();
        newCategory.setNameCategory(nameCategory);
        newCategory.setUsers(List.of(userFromDb));
        Category savedCategory = categoryRepositories.save(newCategory);
        categoryDtoResponse.setId(savedCategory.getCategoryId());
        categoryDtoResponse.setCategoryName(savedCategory.getNameCategory());

        return categoryDtoResponse;
    }
}
