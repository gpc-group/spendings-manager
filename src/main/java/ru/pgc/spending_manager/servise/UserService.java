package ru.pgc.spending_manager.servise;

import ru.pgc.spending_manager.servise.dto.UserDtoResponse;
import ru.pgc.spending_manager.servise.entities.Category;

import java.util.List;
import java.util.UUID;

public interface UserService {
    UUID createUser();

    UserDtoResponse findById(UUID id);

    List<Category> getAllUserCategories(String recipientId);


}
