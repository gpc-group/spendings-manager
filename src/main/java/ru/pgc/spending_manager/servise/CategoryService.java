package ru.pgc.spending_manager.servise;

import ru.pgc.spending_manager.servise.dto.CategoryDtoResponse;

public interface CategoryService {

    CategoryDtoResponse addCategory(String userId, String nameCategory);
}
