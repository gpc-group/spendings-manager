package ru.pgc.spending_manager.servise.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
public class UserDtoResponse {

    private UUID id;

}
