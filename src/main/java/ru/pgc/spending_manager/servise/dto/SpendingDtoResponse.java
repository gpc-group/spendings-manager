package ru.pgc.spending_manager.servise.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
public class SpendingDtoResponse {

    private UUID id;

    private String categoryName;

    private LocalDate expenseDate;

    private double expenseSize;
}
