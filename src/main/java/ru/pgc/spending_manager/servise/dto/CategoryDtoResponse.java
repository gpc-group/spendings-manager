package ru.pgc.spending_manager.servise.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;
@NoArgsConstructor
@Getter
@Setter
public class CategoryDtoResponse {

    private UUID id;

    private String categoryName;

}
