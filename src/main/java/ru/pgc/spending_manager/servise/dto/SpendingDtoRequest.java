package ru.pgc.spending_manager.servise.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.UUID;


@Getter
@Setter
public class SpendingDtoRequest {

    @NotNull
    private UUID userId;

    @NotBlank
    private String categoryName;

    @NotNull
    private LocalDate expenseDate;

    @NotNull
    @Min(1)
    private double expenseSize;


}
