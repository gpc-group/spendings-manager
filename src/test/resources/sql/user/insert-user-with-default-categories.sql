insert into spending_manager.user (id)
values ('96c26469-e6b7-423e-9aba-0c099f4e1428');

insert into spending_manager.category (category_id, name_category, is_default)
values ('f81d4fae-7dec-11d0-a765-00a0c91e6bf6', 'Продукты', true),
       ('7e37b899-8576-4d3d-9652-f517c24a9053', 'Медицина', true),
       ('9d10eeba-1c7e-4bfb-8a97-9f85c1f4a4f6', 'Товары для дома', true),
       ('6f8c6543-8e67-4b0a-bc5e-2f37df598c0e', 'Развлечения', true),
       ('0403173e-de89-4df8-9856-69290f405042', 'Кафе и рестораны', true),
       ('ffdf8b47-3bf9-4e36-803f-1441c10ad2b3', 'Образование', true),
       ('2ac9a211-c3ff-49b7-a4f9-2428df5ccf5a', 'Одежда', true),
       ('b93d0627-7556-4828-85a4-eaeb9d3f3f67', 'Автомобиль', true),
       ('5688c45f-6e31-4050-bbf9-4365b15348f1', 'Вредные привычки', true),
       ('9375f02e-3741-49f5-9984-004e0b637919', 'Чокопай', false);

insert into spending_manager.user_category
values ('f81d4fae-7dec-11d0-a765-00a0c91e6bf6', '96c26469-e6b7-423e-9aba-0c099f4e1428'),
       ('7e37b899-8576-4d3d-9652-f517c24a9053', '96c26469-e6b7-423e-9aba-0c099f4e1428'),
       ('9d10eeba-1c7e-4bfb-8a97-9f85c1f4a4f6', '96c26469-e6b7-423e-9aba-0c099f4e1428'),
       ('6f8c6543-8e67-4b0a-bc5e-2f37df598c0e', '96c26469-e6b7-423e-9aba-0c099f4e1428'),
       ('0403173e-de89-4df8-9856-69290f405042', '96c26469-e6b7-423e-9aba-0c099f4e1428'),
       ('ffdf8b47-3bf9-4e36-803f-1441c10ad2b3', '96c26469-e6b7-423e-9aba-0c099f4e1428'),
       ('2ac9a211-c3ff-49b7-a4f9-2428df5ccf5a', '96c26469-e6b7-423e-9aba-0c099f4e1428'),
       ('b93d0627-7556-4828-85a4-eaeb9d3f3f67', '96c26469-e6b7-423e-9aba-0c099f4e1428'),
       ('5688c45f-6e31-4050-bbf9-4365b15348f1', '96c26469-e6b7-423e-9aba-0c099f4e1428');

