import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.modulith.core.ApplicationModules;
import ru.pgc.spending_manager.SpendingsManager;

class SpendingsManagerTest {
    @Test
    @DisplayName("Проверить целостность модулей")
    void testModulesIntegrity() {

        ApplicationModules.of(SpendingsManager.class).verify();
    }
}
