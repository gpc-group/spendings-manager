package ru.pgc.spending_manager.servise.util_classes;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ResourceUtils {

    private final ObjectMapper objectMapper;

    public ResourceUtils(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public <T> T getObjectFromResources(String resourcesPath, Class<T> clazz) throws IOException {
        return objectMapper.readValue(new ClassPathResource(resourcesPath).getFile(), clazz);
    }
}
