package ru.pgc.spending_manager.servise.repository;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import ru.pgc.spending_manager.servise.entities.Category;
import ru.pgc.spending_manager.servise.entities.Spending;
import ru.pgc.spending_manager.servise.entities.User;
import ru.pgc.spending_manager.servise.util_classes.ResourceUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@Slf4j
class UserRepositoryTest {

    @Autowired
    private ResourceUtils resourceUtils;
    @Autowired
    private UserRepository userRepository;

    @Test
    @DisplayName("Должен вернуть пользователя по id")
    @Sql(value = {"/sql/user/insert-user-with-default-categories.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void findUserById() throws IOException {

        UUID uuid = UUID.fromString("96c26469-e6b7-423e-9aba-0c099f4e1428");
        UUID uuidRandom = UUID.randomUUID();
        log.info(uuidRandom.toString());
        User userFromResources = resourceUtils.getObjectFromResources("/json/user.json", User.class);

        Optional<User> userById = userRepository.findUserById(uuid);

        assertTrue(userById.isPresent());
        assertEquals(userFromResources, userById.get());
    }

    @Test
    void saveUser() {

        List<Category>categories = new ArrayList<>();
        List<Spending>spendings = new ArrayList<>();
        UUID uuid = UUID.randomUUID();
        User user = new User(uuid, categories, spendings);

        User save = userRepository.save(new User(uuid, categories, spendings));

        assertEquals(user,save);
    }


}