package ru.pgc.spending_manager.servise.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.pgc.spending_manager.servise.entities.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class CategoryRepositoriesTest {

    @Autowired
    private CategoryRepositories categoryRepositories;


    @Test
    void findAll() {


        List<Category> categories = new ArrayList<>();
        categories.add(new Category(UUID.fromString("f81d4fae-7dec-11d0-a765-00a0c91e6bf6"), "Продукты", true, List.of(), List.of()));
        categories.add(new Category(UUID.fromString("7e37b899-8576-4d3d-9652-f517c24a9053"), "Медицина", true, List.of(), List.of()));
        categories.add(new Category(UUID.fromString("9d10eeba-1c7e-4bfb-8a97-9f85c1f4a4f6"), "Товары для дома", true, List.of(), List.of()));
        categories.add(new Category(UUID.fromString("6f8c6543-8e67-4b0a-bc5e-2f37df598c0e"), "Развлечения", true, List.of(), List.of()));
        categories.add(new Category(UUID.fromString("0403173e-de89-4df8-9856-69290f405042"), "Кафе и рестораны", true, List.of(), List.of()));
        categories.add(new Category(UUID.fromString("ffdf8b47-3bf9-4e36-803f-1441c10ad2b3"), "Образование", true, List.of(), List.of()));
        categories.add(new Category(UUID.fromString("2ac9a211-c3ff-49b7-a4f9-2428df5ccf5a"), "Одежда", true, List.of(), List.of()));
        categories.add(new Category(UUID.fromString("b93d0627-7556-4828-85a4-eaeb9d3f3f67"), "Автомобиль", true, List.of(), List.of()));
        categories.add(new Category(UUID.fromString("5688c45f-6e31-4050-bbf9-4365b15348f1"), "Вредные привычки", true, List.of(), List.of()));


        List<Category>categoriesRepositories = categoryRepositories.findAll();

        assertEquals(categories, categoriesRepositories);


    }


}