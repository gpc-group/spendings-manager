package ru.pgc.spending_manager.servise.impl;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import ru.pgc.spending_manager.servise.CategoryService;
import ru.pgc.spending_manager.servise.dto.CategoryDtoResponse;
import ru.pgc.spending_manager.servise.entities.Category;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CategoryServiceImplTest {

    @Autowired
    private CategoryService categoryService;

    @Test
    @Sql(value = {"/sql/user/delete-all.sql", "/sql/user/insert-user-with-default-categories.sql"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = {"/sql/user/delete-all.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    @DisplayName("Должен добваить новую категорию")
    void addNewCategory() {

        Category CategoryExpect = new Category();
        CategoryExpect.setCategoryId(UUID.fromString("96c26469-e6b7-423e-9aba-0c099f4e1998"));
        CategoryExpect.setNameCategory("Чаевые");

        CategoryDtoResponse categoryDtoResponse = categoryService
                .addCategory("96c26469-e6b7-423e-9aba-0c099f4e1428", "Чаевые");


        assertEquals(CategoryExpect.getNameCategory(), categoryDtoResponse.getCategoryName());


    }

    @Test
    @Sql(value = {"/sql/user/delete-all.sql", "/sql/user/insert-user-with-default-categories.sql"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = {"/sql/user/delete-all.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    @DisplayName("Должен вернуть id уже существующей категории")
    void notAddNewCategory() {

        Category categoryExpect = new Category();
        categoryExpect.setCategoryId(UUID.fromString("96c26469-e6b7-423e-9aba-0c099f4e1998"));
        categoryExpect.setNameCategory("Продукты");

        CategoryDtoResponse categoryDtoResponse = categoryService
                .addCategory("96c26469-e6b7-423e-9aba-0c099f4e1428", "Продукты");

        assertEquals(categoryExpect.getNameCategory(), categoryDtoResponse.getCategoryName());

    }
    @Test
    @Sql(value = {"/sql/user/delete-all.sql", "/sql/user/insert-user-with-default-categories.sql"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = {"/sql/user/insert-new-category-without-bindings.sql"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = {"/sql/user/delete-all.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    @DisplayName("Должен вернуть id уже существующей категории")
    void addExistingCategory() {

        Category categoryExpect = new Category();
        categoryExpect.setCategoryId(UUID.fromString("96c26469-e6b7-423e-9aba-0c099f4e1998"));
        categoryExpect.setNameCategory("Техника");

        CategoryDtoResponse categoryDtoResponse = categoryService
                .addCategory("96c26469-e6b7-423e-9aba-0c099f4e1428", "Техника");

        assertEquals(categoryExpect.getNameCategory(), categoryDtoResponse.getCategoryName());

    }


}