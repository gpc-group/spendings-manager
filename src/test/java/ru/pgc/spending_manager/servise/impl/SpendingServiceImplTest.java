package ru.pgc.spending_manager.servise.impl;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import ru.pgc.spending_manager.servise.dto.SpendingDtoRequest;
import ru.pgc.spending_manager.servise.dto.SpendingDtoResponse;
import ru.pgc.spending_manager.servise.excepcions.UserNotFoundException;

import java.time.LocalDate;
import java.util.NoSuchElementException;
import java.util.UUID;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class SpendingServiceImplTest {

    @Autowired
    private SpendingServiceImpl spendingServiceImpl;

    @Test
    @Sql(value = {"/sql/user/delete-all.sql", "/sql/user/insert-user-with-default-categories.sql"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = {"/sql/user/delete-all.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    @DisplayName("Должен успешно добавить трату")
    void addSpending() {

        SpendingDtoRequest spendingDtoRequest = new SpendingDtoRequest();
        spendingDtoRequest.setUserId(UUID.fromString("96c26469-e6b7-423e-9aba-0c099f4e1428"));
        spendingDtoRequest.setCategoryName("Продукты");
        spendingDtoRequest.setExpenseDate(LocalDate.of(2024,6,26));
        spendingDtoRequest.setExpenseSize(1000L);

        SpendingDtoResponse spendingExpect = new SpendingDtoResponse();
        spendingExpect.setCategoryName("Продукты");
        spendingExpect.setExpenseDate(LocalDate.of(2024,6,26));
        spendingExpect.setExpenseSize(1000L);

        SpendingDtoResponse spendingActual = spendingServiceImpl.addSpending(spendingDtoRequest);

        assertEquals(spendingExpect.getCategoryName(), spendingActual.getCategoryName());
        assertEquals(spendingExpect.getExpenseDate(), spendingActual.getExpenseDate());
        assertEquals(spendingExpect.getExpenseSize(), spendingActual.getExpenseSize());


    }

    @Test
    @Sql(value = {"/sql/user/delete-all.sql", "/sql/user/insert-user-with-default-categories.sql"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = {"/sql/user/delete-all.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    @DisplayName("Должен выбросить исключение UserNotFoundException")
    void addSpendingNonExistingUser() {

        SpendingDtoRequest spendingDtoRequest = new SpendingDtoRequest();
        spendingDtoRequest.setUserId(UUID.fromString("96c26469-e6b7-423e-9aba-0c099f4e1430"));
        spendingDtoRequest.setCategoryName("Продукты");
        spendingDtoRequest.setExpenseDate(LocalDate.of(2024,6,26));
        spendingDtoRequest.setExpenseSize(1000L);

        assertThrows(UserNotFoundException.class, () -> spendingServiceImpl.addSpending(spendingDtoRequest));



    }

    @Test
    @Sql(value = {"/sql/user/delete-all.sql", "/sql/user/insert-user-with-default-categories.sql"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = {"/sql/user/delete-all.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    @DisplayName("Должен выбросить исключение NotSuchElementException")
    void addSpendingNonExistingCategory() {

        SpendingDtoRequest spendingDtoRequest = new SpendingDtoRequest();
        spendingDtoRequest.setUserId(UUID.fromString("96c26469-e6b7-423e-9aba-0c099f4e1428"));
        spendingDtoRequest.setCategoryName("Чокопай");
        spendingDtoRequest.setExpenseDate(LocalDate.of(2024,6,26));
        spendingDtoRequest.setExpenseSize(1000L);

        assertThrows(NoSuchElementException.class, () -> spendingServiceImpl.addSpending(spendingDtoRequest));



    }
}